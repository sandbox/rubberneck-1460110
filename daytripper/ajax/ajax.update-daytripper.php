<?php
/*
*@file ajax.update-skeduale.php
*
*/

/* Posts an array of the current events for a user to
*  a system variable called 'daytripper_agenda' with variable_set
*
*
*/
function ajax_update_daytripper( $data_in ){

$daytripper_agenda['sid'] = session_id();
$data_in =  mysql_real_escape_string( $data_in );
$i = 0;
$events = explode( '|', $data_in );
if( is_array( $events ) && !empty( $events )  ){
	foreach( $events AS $event ){
	    $ev = explode( '~', $event );
	    if( is_array( $ev) && !empty( $ev) && trim($ev[0]) != '' ){
				$daytripper_agenda[$i]['eventID'] =  str_replace( 'daytripper-', '', $ev[0] ) ;
				$daytripper_agenda[$i]['sched_hour'] = $ev[1] ;
				$i++;
	    }
	}//foreach
	if( is_array( $daytripper_agenda ) && !empty( $daytripper_agenda ) ){
	      variable_set( 'daytripper_agenda', $daytripper_agenda );
	}
}//if
echo 'variable_get( daytripper_agenda )<pre>';
print_r( variable_get( 'daytripper_agenda', $daytripper_agenda )  );
echo '</pre>';
}