/**
 * @preserve
 * Day Tripper v1.0
 * http://rubberneckdesigns/
 *
 * Use fullcalendar.css for basic styling.
 * For event drag & drop, requires jQuery UI draggable.
 * For event resizing, requires jQuery UI resizable.
 *
 *
 *
 *
 */


(function ($) {
	
  Drupal.behaviors.daytripper = {
    attach: function (context, settings) {
    // START ///////////////  DAY TRIPPER jQuery Behaviors ///////////////////
    if(  $('.daytripper-event').length > 0    ){
					$('.daytripper-event').draggable({
							create: function( event , ui ) {     
															var eventID  = $(this).attr('id');
															var X_val     = $(this).offset().left;
															var Y_val     = $(this).offset().top;
													},
							revert: 'invalid',
							cursor: 'move', 
							scroll: true,
							zIndex: 999, 
							snap: '.hour', 
							helper: 'original'
					});     
     }// if(  $('.agenda-event').length > 0
   if(  $('.daytripperDroppable').length > 0    ){
      $('.daytripperDroppable').droppable( {
							over: function(event, ui) { 
									// Display this agenda time slot time in the event item box
									//var time = $(this).parent().find('.time-label').text();
								//	ui.helper.find('.daytripper-hover-info').html( time );
							},
							out: function(event, ui) { 
									//$(this).removeClass('over-agenda');
									// Remove this agenda time slot time in the event item box
								//	ui.helper.find('.daytripper-hover-info').html(' ');
								//	$('.ui-draggable-dragging').removeClass('over-agenda');
							},
						 drop:    daytripperHandleDropEvent

			 });
   }// if(  $('.daytripperDroppable').length > 0 

		 
		// END ///////////////  daytripper jQuery Behaviors ///////////////////
    }
  };
  



function isOverlapping(event, ui, eventlistSelector  ){
//Reset the droppable revert option.
ui.draggable.draggable( "option", "revert", 'invalid' );
// Configure some draggable variables
 var thisDraggableClass = ui.draggable.attr('class');	
 var thisDraggableElem = ui.draggable;		
 var thisDraggableID = ui.draggable.attr('id');
// Configure the X,Y coordinates of the current draggable
var thisX1= ui.draggable.offset().left;
var thisY1 = ui.draggable.offset().top;
var thisX2 = ( thisX1 + ui.draggable.width() );
var thisY2 = ( thisY1 + ui.draggable.height() ) ;
// Iterate over the event elements and prevent drop if it is overlapping.
$( eventlistSelector  ).each(function(i, e){
   //Configure the current droppable's ID
		var droppableID = $(this).attr('id')  ;
		if( thisDraggableID != droppableID ){//event excludes itself here
		//Configure the variables for the X,Y coordinates of each event
		var eX1= $(this).offset().left;
		var eY1 = $(this).offset().top;
		var eX2 = ( eX1 + $(this).width() );
		var eY2 = ( eY1 + $(this).height()  );				
		//This next IF statement decides if the the current draggable
		// and any of the previouse dropped events are over lapping
			if( ( ( thisY1.toFixed(1) <= eY1.toFixed(1) && 
							thisY2.toFixed(1)  >= eY1.toFixed(1) ) || 
						( thisY1.toFixed(1) >= eY1.toFixed(1) && 
							thisY1.toFixed(1)  <= eY2.toFixed(1) ) || 
						( thisY1.toFixed(1) <= eY1.toFixed(1) &&  
							thisY2.toFixed(1)  >= eY2.toFixed(1) ) || 
						( thisY1.toFixed(1) >= eY1.toFixed(1) && 
							thisY2.toFixed(1) <= eY2.toFixed(1) )    )
					&& 
				 ( ( thisX1.toFixed(1) <= eX1.toFixed(1) && 
						 thisX2.toFixed(1)  >= eX1.toFixed(1) ) || 
					(  thisX1.toFixed(1) >= eX1.toFixed(1) && 
						 thisX1.toFixed(1) <= eX2.toFixed(1) )	|| 
					 ( thisX1.toFixed(1) <= eX1.toFixed(1) && 
						 thisX2.toFixed(1) >= eX2.toFixed(1) )  || 
					 ( thisX1.toFixed(1) >= eX1.toFixed(1) && 
						 thisX2.toFixed(1) <= eX2.toFixed(1) )     )   ){		
						 // If it is overlapping another event element then 
						 // make the droppable revert: true
						 // ( Go back to where it came from ).
								ui.draggable.draggable( "option", "revert", true );
								return false;
			}// end if is overlapping
			else {  return true;  }
		}// end if dropaable is not self
	}); //each
}// else


function daytripperHandleDropEvent( event, ui ) {
// Configure the event list to be examined by  isOverlapping()
var eventlistSelector = '.view-agenda-events li .agenda-event';
// isOverlapping() prevetns the drop if it is overlapping other events
isOverlapping(event,ui, eventlistSelector );
 
}

function daytripperHelper(event){
// intentionally empty
}

function daytripperHandleDragStop( event, ui ) {
// intentionally empty
}
  
})(jQuery);










