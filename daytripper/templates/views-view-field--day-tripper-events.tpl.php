<?php
 /**
  * This template is used to print a single field in a view. It is not
  * actually used in default Views, as this is registered as a theme
  * function which has better performance. For single overrides, the
  * template is perfectly okay.
  *
  * Variables available:
  * - $view: The view object
  * - $field: The field handler object that can process the input
  * - $row: The raw SQL result that can be used
  * - $output: The processed output that will normally be used.
  *
  * When fetching output from the $row, this construct should be used:
  * $data = $row->{$field->field_alias}
  *
  * The above will guarantee that you'll always get the correct data,
  * regardless of any changes in the aliasing that might happen if
  * the view is modified.
  */
?>
<?php
// Configure Variables
// From the Administration panel
$daytripper_minutes_per_pixels = variable_get( 'daytripper_minutes_per_pixels', 2 );
$daytripper_how_many_hours = variable_get( 'daytripper_how_many_hours', 10 );
$daytripper_start_hour = variable_get( 'daytripper_start_hour', 8 );
//Calculate the ratio of pixels per minutes 
//(for calculating where on the agenda to put the event)
$px_min_ratio = 1/ $daytripper_minutes_per_pixels;

//drupal_set_message('<pre>' . print_r(  $row->_field_data['nid']['entity'], TRUE) . '</pre>', $type = 'status');

//Configure the data from the $row variable
$field_dat = $row->_field_data['nid']['entity'] ;// object
//  [value] => 2012-01-29 08:00:00
$date_time_satrt = $field_dat->field_date['und'][0]['value'];
//  [value2] => 2012-01-29 15:00:00
$date_time_end = $field_dat->field_date['und'][0]['value2'];
$unix_date_time_satrt = strtotime($date_time_satrt);
$unix_date_time_end = strtotime($date_time_end);
$date_satrt = date( 'M d, y', $unix_date_time_satrt );
$time_start = date( 'H:i', $unix_date_time_satrt );
$date_end = date( 'M d, y', $unix_date_time_end );
$time_end = date( 'H:i', $unix_date_time_end);
//Seperate the time from the date
$hrs_mins_start = explode( ":", $time_start );
$hrs_mins_end = explode( ":", $time_end );
//Calculate how many minutes for a give HH:mm format
$mins_start = (($hrs_mins_start[0] * 60 )+$hrs_mins_start[1]);
$mins_end = (($hrs_mins_end[0] * 60 )+$hrs_mins_end[1]);
$duration = ( $mins_end - $mins_start );
//Calculate the base hours for offsetting the number of pixels
$base_hours = ( $daytripper_start_hour * 60 * $px_min_ratio );
////Calculate base adjust number (if any)
$base_adjust = 0;
//Calculate the base offset number
$pos_base = -( $base_hours - $base_adjust );
//Calculate the "Y" position of the event
// DEVE: print "Bas_pos:".$pos_base ." + (  mins_start:".$mins_start." *  " . $px_min_ratio.  " ) <br />" ;
$ypx = ( $pos_base + ceil($mins_start * $px_min_ratio ) ) ;
$duration_px = ( ceil($duration * $px_min_ratio ) ) ;
// Create the inline style info
$ystart_offset = ' top: ' . $ypx . 'px; ';
$duration_ht = ' height: ' . $duration_px . 'px; ';
$style_event = 'style="position:absolute; ' . $ystart_offset . '" ';

// Address Info
//drupal_set_message('<pre>' . print_r( $field_dat , TRUE) . '</pre>', $type = 'status');
$addr1 = ( !empty($field_dat->locations[0]['street']) )? 
                 $field_dat->locations[0]['street']: ' ' ;
$addr2 = ( !empty($field_dat->locations[0]['additional']) )? 
                 $field_dat->locations[0]['additional']: ' ' ;
$city = ( !empty($field_dat->locations[0]['city']) )? 
               $field_dat->locations[0]['city']: ' ' ;
$state= ( !empty($field_dat->locations[0]['province']) )? 
               $field_dat->locations[0]['province']: ' ' ;
$addr = ( trim( $addr2 ) == '' )? $addr1: $addr1 . ', ' . $addr2 ;
$address = $addr . ', ' . $city  . ', ' . $state;

?>

<div <?php echo $style_event;  ?> id="daytripper-<?php echo $field_dat->nid; ?>" class="daytripper-event">
<div class="daytripper-checkbox"><input type="checkbox" value="<?php echo $field_dat->nid; ?>" /></div>
<span class="title"><?php echo $field_dat->title; ?></span>
<div class="date"><?php echo $date_satrt; ?> <span class="time"><?php echo $time_start; ?> to <?php echo $time_end; ?></span></div>
<div class="daytripper-addr-info"><?php echo $address; ?></div>
</div>
