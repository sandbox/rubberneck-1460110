<?php
/*
Day Tripper Agenda template

*/
// Get the admin settings
$daytripper_minutes_per_pixels = variable_get( 'daytripper_minutes_per_pixels', 2 );
$daytripper_how_many_hours = variable_get( 'daytripper_how_many_hours', 10 );
$daytripper_start_hour = variable_get( 'daytripper_start_hour', 8 );
// Calculate the ht of the divs representing hours on the agenda
$ht = ( 60 / $daytripper_minutes_per_pixels  -1 );//-1 allows for board on .hour
// Calculate the last hour to show on the agenda
$last_hr = abs( ( $daytripper_start_hour + $daytripper_how_many_hours ) );
$last_hr = ( $last_hr <= 24 )? $last_hr : 24;
// Calculate the total height of the agenda div#daytripper_day
$ttl_ht = ( ( $daytripper_how_many_hours + 0 ) * ceil($ht)  );
?>

<div id='daytripper-wrap'>
<p class="daytripper-instructions">Check the boxes on the events to "lock" it onto the itinerary</p>
<label for "my-start-address" >My current location</lable>
<input id="my-start-address" type="textbox" value="" />
	<div id='daytripper_day' style="height: <?= (ceil( $ttl_ht) + 60 ) ?>px"> 
	
		<div id='daytripper-hr-labels'> 
		<?php
		//Layout the agenda markup
		for ( $i = $daytripper_start_hour; $i <= $last_hr; $i++ ) {
		echo '<div class="time-label"  style="height: '. $ht .'px">' . $i . ':00</div> ';
		} 
		?>
		</div><!--    id='daytripper-hr-labels'  -->		
		<div id='daytripper-agenda' class=""> 
		<?php
		// Events list created with Views as a block
		$block = module_invoke('views', 'block_view', 'day_tripper_events-block_1');
	  	print render($block);
		?>
		<?php
			//Layout the agenda markup
			for ( $ii = $daytripper_start_hour; $ii <= $last_hr; $ii++ ) {
			echo '<div id="hr-'.$ii.'" class="hour agendaDroppable" style="height: '. $ht .'px">
			</div>' . "\n";
			}
		?>
		</div><!--    id='daytripper-agenda'  -->
		
	</div><!--    id='daytripper_day'  -->
	<div id='daytripper-center-console'> 	
	    <input id="but-add-event" type="button" value="Add Event" />
	    <input id="but-add-recalc" type="button" value="Recalculate" />
	    <input id="but-add-save" type="button" value="Save" />
	    <input id="but-add-print" type="button" value="Print" />
	    <input id="but-add-email" type="button" value="Email" />
	</div><!--     id='daytripper-center-console'  -->
	<div id='daytripper-map-console'> 	
	 <div id="map_canvas" style="width:350px; height:350px"> </div><!--  id="daytripper-map"  -->	
	    <input id="but-add-directions" type="button" value="Directions" />
	       <div id="warnings_panel"> </div><!--  id="directions_panel"  -->
	    	 <div id="directions_panel"> </div><!--  id="directions_panel"  -->
	</div><!--     id='daytripper-map-console'  -->

	
<input type="hidden" id="daytripper_minutes_per_pixels" value="<?= 
                                                       $daytripper_minutes_per_pixels ?>" />
<input type="hidden" id="daytripper_how_many_hours" value="<?= 
                                                       $daytripper_how_many_hours ?>" />
<input type="hidden" id="daytripper_start_hour" value="<?= 
                                                       $daytripper_start_hour ?>" />

<div id="daytripper-agenda-data"> </div>
</div>
