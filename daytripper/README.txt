

NOTE: For the drag-n-drop to work there can only be on position:relative in the ancestry of the draggable event elements. If you use a theme that has a position:relative in the ancestry of the draggable then you will need to adjust for it in some way:
A) Add the appropriate number of pixels to the $base_adjust variable (iether in the views template or the admin).
B) Override the position relative in the ancestory of the theme with a theme override function.

This module may create a Views module block that you will be able to see in the blocks admin in the list of blocks that have not been enabled. DO NOT enable this block from the blocks admin. You may ignore it and the module will work fine.

