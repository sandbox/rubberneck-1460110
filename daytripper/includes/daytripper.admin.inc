<?php

/**
 * Settings form.
 */
function pifm_settings_form($form, &$form_state) {

$pifm_minutes_per_pixels = variable_get( 'pifm_minutes_per_pixels', 2 );
$pifm_how_many_hours = variable_get( 'pifm_how_many_hours', 10 );
$pifm_start_hour = variable_get( 'pifm_start_hour', 8 );


	$form['pifm_how_many_hours'] = array(
	'#title' => t('How many hours in the agenda (recomended is 10)?'),
	'#type' => 'textfield',
	'#default_value' => $pifm_how_many_hours,
	'#size' => 3,
	'#maxlength' =>3,
	);
	$form['pifm_start_hour'] = array(
	'#title' => t('What is the first hour on the agenda?'),
	'#type' => 'textfield',
	'#default_value' => $pifm_start_hour,
	'#size' => 3,
	'#maxlength' =>3,
	);	
	$form['pifm_minutes_per_pixels'] = array(
	'#title' => t('How many minutes does each vertical pixel represent in the agenda (recomended is 2)?'),
	'#type' => 'textfield',
	'#default_value' => $pifm_minutes_per_pixels,
	'#size' => 3,
	'#maxlength' =>3,
	'#description' => t("Please enter the vertical pixels per minute for the agenda.", array('%front' => '<front>')),
	);

$form['submit'] = array(
  '#type' => 'submit', 
  '#value' => t('Submit')
  );
	
	//return $form;
	return system_settings_form($form);
  
}

/**
 * Settings form validate.
 */
function pifm_settings_form_validate($form, &$form_state) {
  
}

 
/**
 * Settings form submit.
 */
function pifm_settings_form_submit($form, &$form_state) {

variable_set( 'pifm_minutes_per_pixels', $form_state['values']['pifm_minutes_per_pixels'] );
variable_set( 'pifm_how_many_hours', $form_state['values']['pifm_how_many_hours'] );
variable_set( 'pifm_start_hour', $form_state['values']['pifm_start_hour'] );
	
}